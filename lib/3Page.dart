import 'package:flutter/material.dart';

import 'main.dart';

class ThreePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("リスト"),
      ),
      body: Center(
        child: Center(
          child: Column(
            children: <Widget>[

              Text("Navigator.defaultRouteName : " + Navigator.defaultRouteName),

              RaisedButton(
                child: Text('pop'),
                onPressed: (){
                  Navigator.pop(context);
                },
              ),

              RaisedButton(
                child: Text('push'),
                onPressed: (){
                  Navigator.pushNamed(context, '/4');
                },
              ),

              RaisedButton(
                child: Text('popUntil + pushNamed'),
                onPressed: (){
                  Navigator.popUntil(context, ModalRoute.withName('/2'));
                  Navigator.pushNamed(context, '/4');
                },
              ),




            ],
          ),
        ),
      ),
    );
  }
}
