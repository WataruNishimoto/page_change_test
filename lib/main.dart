import 'package:flutter/material.dart';

import '2Page.dart';
import '3Page.dart';
import '4Page.dart';

void main() {
  runApp(MaterialApp(
    title: 'Named Routes Demo',
    initialRoute: '/',
    routes: {
      '/': (context) => FirstScreen(),
      '/2': (context) => TwoPage(),
      '/3': (context) => ThreePage(),
      '/4': (context) => FourPage(),
    },
  ));
}

class FirstScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('1'),
      ),
      body: Center(
        child: Center(
          child: Column(
            children: <Widget>[

              Text("Navigator.defaultRouteName : " + Navigator.defaultRouteName),

              RaisedButton(
                child: Text('push'),
                onPressed: (){
                  Navigator.pushReplacementNamed(context, "/2");
                },
              ),

            ],
          ),
        ),
      ),
    );
  }
}
