import 'package:flutter/material.dart';

class FourPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("詳細"),
      ),
      body: Center(
        child: Center(
          child: Column(
            children: <Widget>[

              Text("Navigator.defaultRouteName : " + Navigator.defaultRouteName),

              RaisedButton(
                child: Text('pop'),
                onPressed: (){
                  Navigator.pop(context);
                },
              ),

              RaisedButton(
                child: Text('push'),
                onPressed: (){
                  Navigator.pushNamed(context, '/5');
                },
              ),

              RaisedButton(
                child: Text('popAndPushNamed(現在のページを削除して指定ページをPush)'),
                onPressed: (){
                  Navigator.popAndPushNamed(context, '/2');
                },
              ),



            ],
          ),
        ),
      ),
    );
  }
}
